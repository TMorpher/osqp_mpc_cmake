cmake_minimum_required(VERSION 3.2)
project(osqp_eigen_simplempc)

find_package(OsqpEigen REQUIRED)
find_package(Eigen3 REQUIRED)

# target_include_directories(simplempc PRIVATE ${PYTHON_INCLUDE_DIRS})

include_directories(
    # ${OMPL_INCLUDE_DIRS}
    ${EIGEN3_INCLUDE_DIRS}
    # ${MathGL_INCLUDE_DIRS}
)
add_executable(slide_point_mpc src/mpc_code.cpp)
target_link_libraries(slide_point_mpc OsqpEigen::OsqpEigen)

