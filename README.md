## 导轨-滑块模型的MPC控制例程

* 把MPC问题转为最优化问题，使用OSQP-Eigen开源库，求解最优化问题。

## 使用方法

* 进入仓库目录(默认目录名称为 `osqp_mpc_cmake`)

```
cd osqp_mpc_cmake
```

* 创建 `build`文件夹并进入

```
mkdir build & cd build
```

* 运行 `cmake`生成 `Makefile`

```
cmake ..
```

* 运行make，开始编译

```
make
```

* 等待编译完成，得到可执行文件 `slide_point_mpc`并运行

```
./slide_point_mpc
```

* 回到 `src`文件夹，运行display_data.py脚本

```
python3 display_data.py
```

* 显示运行结果
  ![](result.png)

## 参考

[1] https://github.com/robotology/osqp-eigen
