import numpy as np
import matplotlib.pyplot as plt

t = np.arange(1000) * 0.032
pos = np.sin(t) + 0.5 * 0.01 * t * t
vel = np.cos(t) + 0.01 * t

plt.plot(t, pos)
plt.plot(t, vel)
plt.show()

with open("../build/trace_data.csv", "w", encoding="utf-8") as f:
    # f.write("%s,%s\n"%("position","velocity"))
    for i in range(1000):
        f.write("%d,%f,%f\n"%(i, pos[i], vel[i]))
